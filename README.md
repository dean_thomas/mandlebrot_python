# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Computes the Mandelbrot fractal.

### How do I get set up? ###

Requires the following Python3 libraries.

* Numpy
* MatPlotLib
* PyQt5
* PyOpenCl

Also requires drivers for OpenCL