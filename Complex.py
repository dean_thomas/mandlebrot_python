from decimal import *
from math import sqrt


class Complex:
    """
    Complex number class with arbitrary precision decimals
    """
    __re__ = Decimal(0)
    __im__ = Decimal(0)

    def __init__(self):
        self.__re__ = Decimal(0)
        self.__im__ = Decimal(0)

    def __init__(self, re, im=0):
        self.__re__ = Decimal(re)
        self.__im__ = Decimal(im)

    def __str__(self):
        if self.__im__.is_signed():
            sign = "-"
        else:
            sign = "+"

        return "{0}{1}{2}i".format(self.__re__, sign, abs(self.__im__))

    def __repr__(self):
        return self.__str__()

    def __add__(self, other):
        return Complex(self.__re__ + other.__re__, self.__im__ + other.__im__)

    def __sub__(self, other):
        return Complex(self.__re__ - other.__re__, self.__im__ - other.__im__)

    def __mul__(self, other):
        #   (a+bi)(c+di) = (ac-bd) + (bc+ad)i
        return Complex((self.__re__*other.__re__)-(self.__im__*other.__im__),
                       (self.__im__*other.__re__)+(self.__re__*other.__im__))

    def __abs__(self):
        return sqrt(self.__re__**2 + self.__im__**2)

    def re(self):
        return self.__re__

    def im(self):
        return self.__im__