from Complex import Complex
import numpy as np
import matplotlib.pyplot as plt
import sys
import time
from functools import partial
from gui import ConfigDialog
from PyQt5 import QtCore, QtGui, QtWidgets, uic

from opencl_test import calc_fractal_opencl

#   Get the default MatPlotLib colour map
colour_map = plt.rcParams['image.cmap']

max_iterations = 50
escape_radius = 8

x_range = (-2.5, 1.5)
y_range = (-1.5, 1.5)

x_steps = 1000
y_steps = 1000

USE_OPEN_CL = True
#USE_OPEN_CL = False

def compute_mandelbrot(type):
    x_min = x_range[0]
    x_max = x_range[1]
    y_min = y_range[0]
    y_max = y_range[1]

    print("Computing Mandelbrot fractal x = [{0}, {1}], y = [{2}, {3}]".format(x_min,
                                                                               x_max,
                                                                               y_min,
                                                                               y_max))

    z_re = np.linspace(x_min, x_max, x_steps, endpoint=True)
    z_im = np.linspace(y_min, y_max, y_steps, endpoint=True)
    print("Using {0} samples on x-axis".format(len(z_re)))
    print("Using {0} samples on y-axis".format(len(z_im)))

    iterations = np.zeros(shape=(len(z_re),len(z_im)),dtype=int)
    x = 0
    for re in z_re:
        y = 0
        for im in z_im:
            iterations[x,y] = mandelbrot_kernel(type(re, im))
            y = y+1
        x = x+1

    return iterations


def do_update():
    global colour_map, data
    global x_range, x_steps
    global y_range, y_steps

    colour_map = dialog.get_colour_map()
    if dialog.is_modified():
        x_range = dialog.get_x_range()
        x_steps = dialog.get_x_step()
        y_range = dialog.get_y_range()
        y_steps = dialog.get_y_step()
        data = compute_mandelbrot(Complex)

    show_plot()


def show_config_dialog():
    #   show configuration
    dialog.set_re_axis(x_range, x_steps)
    dialog.set_im_axis(y_range, y_steps)
    dialog.set_kernel_settings(max_iterations, escape_radius)
    dialog.set_colour_map(colour_map)
    dialog.apply_clicked.connect(do_update)
    if dialog.exec_():
        dialog.disconnect()
        do_update()


def key_press_event(event):
    if event.key == 'c':
        show_config_dialog()
    if event.key == 'x' or event.key == 'q':
        #   exit application
        sys.exit(0)


fig = plt.figure(0)
fig.canvas.mpl_connect('key_press_event', key_press_event)
fig.clf()


def show_plot():
    #fig, ax = plt.subplots()
    #plt.ion()

    #   Data points for plot
    z_re = np.linspace(x_range[0], x_range[1], x_steps, endpoint=True)
    z_im = np.linspace(y_range[0], y_range[1], y_steps, endpoint=True)

    #   Plot the data using the supplied sample points
    plt.pcolormesh(z_im, z_re, data.transpose(), cmap=colour_map)

    plt.xlabel("$\Re(z)$")
    plt.ylabel("$\Im(z)$")

    #plt.pcolor(data.transpose(),cmap=colour_map )
    plt.colorbar()
    plt.show()


def mandelbrot_kernel(z):
    #assert(type(z) == Complex)
    c = z

    for i in range(0, max_iterations):
        z = z*z + c

        if abs(z) > escape_radius:
            return i
    return 0


if __name__ == '__main__':
    global app, dialog
    global data

    #plt.ion()
    #app = QtWidgets.QApplication(sys.argv)
    #dialog = ConfigDialog(list(m for m in plt.cm.datad if not m.endswith("_r")))
    if USE_OPEN_CL:
        x1 = x_range[0]
        x2 = x_range[1]
        y1 = y_range[0]
        y2 = y_range[1]
        w = x_steps
        h = y_steps

        #   setup input array (i.e. z_0)
        xx = np.arange(x1, x2, (x2 - x1) / w)
        yy = np.arange(y2, y1, (y1 - y2) / h) * 1j
        q = np.ravel(xx+yy[:, np.newaxis]).astype(np.complex64)

        start_main = time.time()
        values = calc_fractal_opencl(q, max_iterations)
        end_main = time.time()
        secs = end_main - start_main

        #   transpose could be removed if the data is re-arranged
        data = np.reshape(values, (w, h)).astype(np.uint8).transpose()
        # data = (np.reshape(values,(w,h)) / float(values.max()) * 255.).astype(np.uint8)
    else:
        start_main = time.time()
        data = compute_mandelbrot(Complex)
        end_main = time.time()
        secs = end_main - start_main

    print("Main took", secs)
    show_plot()
    plt.show()

    #sys.exit(app.exec_())
