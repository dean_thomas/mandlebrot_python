from PyQt5 import QtCore, QtGui, QtWidgets, uic


class ConfigDialog(QtWidgets.QDialog):
    # Define a new signal called 'trigger' that has no arguments.
    apply_clicked = QtCore.pyqtSignal()

    def _block_signals_(self, blocked):
        #   Allows blocking of signals for dialog set up
        self.ui.doubleSpinBoxMinX.blockSignals(blocked)
        self.ui.doubleSpinBoxMaxX.blockSignals(blocked)
        self.ui.spinBoxCountX.blockSignals(blocked)
        self.ui.doubleSpinBoxMinY.blockSignals(blocked)
        self.ui.doubleSpinBoxMaxY.blockSignals(blocked)
        self.ui.spinBoxCountY.blockSignals(blocked)
        self.ui.horizontalSliderMaxIterations.blockSignals(blocked)
        self.ui.horizontalSliderEscapeRadius.blockSignals(blocked)

    def __init__(self, colour_maps=[]):
        super(ConfigDialog, self).__init__()
        self._is_modified = False
        self.ui = uic.loadUi('ConfigDialog.ui', self)
        self._block_signals_(True)
        self.ui.comboBoxColourMaps.addItems(colour_maps)

    def set_re_axis(self, range_x, count_x):
        self.ui.doubleSpinBoxMinX.setValue(range_x[0])
        self.ui.doubleSpinBoxMaxX.setValue(range_x[1])
        self.ui.spinBoxCountX.setValue(count_x)

    def get_x_range(self):
        return self.ui.doubleSpinBoxMinX.value(), self.ui.doubleSpinBoxMaxX.value()

    def get_x_step(self):
        return self.ui.spinBoxCountX.value()

    def set_im_axis(self, range_y, count_y):
        self.ui.doubleSpinBoxMinY.setValue(range_y[0])
        self.ui.doubleSpinBoxMaxY.setValue(range_y[1])
        self.ui.spinBoxCountY.setValue(count_y)

    def get_y_range(self):
        return self.ui.doubleSpinBoxMinY.value(), self.ui.doubleSpinBoxMaxY.value()

    def get_y_step(self):
        return self.ui.spinBoxCountY.value()

    def set_kernel_settings(self, max_iterations, escape_radius):
        self.ui.horizontalSliderMaxIterations.setValue(max_iterations)
        self.ui.horizontalSliderEscapeRadius.setValue(escape_radius)

    def set_colour_map(self, colour_map):
        self.ui.comboBoxColourMaps.setCurrentText(colour_map)

    def get_colour_map(self):
        return self.ui.comboBoxColourMaps.currentText()

    def is_modified(self):
        return self._is_modified

    def exec_(self):
        #   Re-enable signals
        self._block_signals_(False)
        return super(ConfigDialog, self).exec_()

    @QtCore.pyqtSlot()
    def modified(self):
        self._is_modified = True
        print('Settings were modified')

    @QtCore.pyqtSlot("QAbstractButton*")
    def applyClicked(self, value):
        self.apply_clicked.emit()
