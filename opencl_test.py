#   Original code: https://github.com/pyopencl/pyopencl/blob/master/examples/demo_mandelbrot.py [29-07-2017]
import numpy as np
import pyopencl as cl
import os

os.environ['PYOPENCL_COMPILER_OUTPUT'] = '1'


def list_opencl_devices(platform):
    assert(type(platform) is cl.Platform)

    #   Print list of devices on this platform
    devices = platform.get_devices()
    for device in devices:
        device_name = device.get_info(cl.device_info.NAME)
        print("Device: {0}".format(device_name))

        device_version = device.get_info(cl.device_info.VERSION)
        print("\tVersion: {0}".format(device_version))

        device_vendor = device.get_info(cl.device_info.VENDOR)
        print("\tVendor: {0}".format(device_vendor))

        device_profile = device.get_info(cl.device_info.PROFILE)
        print("\tProfile: {0}".format(device_profile))

        #   Print list of available extensions
        device_extensions = device.get_info(cl.device_info.EXTENSIONS)
        extensions = device_extensions.split()
        print("\tExtensions:")
        for extension in extensions:
            print("\t\t{0}".format(extension))


def list_opencl_platforms():
    platforms = cl.get_platforms()
    for platform in platforms:
        platform_name = platform.get_info(cl.platform_info.NAME)
        print("Platform: {0}".format(platform_name))

        platform_version = platform.get_info(cl.platform_info.VERSION)
        print("\tVersion: {0}".format(platform_version))

        platform_vendor = platform.get_info(cl.platform_info.VENDOR)
        print("\tVendor: {0}".format(platform_vendor))

        platform_profile = platform.get_info(cl.platform_info.PROFILE)
        print("\tProfile: {0}".format(platform_profile))

        #   Print list of available extensions
        platform_extensions = platform.get_info(cl.platform_info.EXTENSIONS)
        extensions = platform_extensions.split()
        print("\tExtensions:")
        for extension in extensions:
            print("\t\t{0}".format(extension))

        #   List devices on this platform
        list_opencl_devices(platform)


def read_opencl_source(filename):
    """
    Opens the OpenCL file and returns its content as a string
    :param filename:
    :return:
    """
    with open(filename, 'r') as input_file:
        data = input_file.read()
    return data


def calc_fractal_opencl(q, max_iter):
    #   list available devices
    list_opencl_platforms()

    #   create a context and command queue
    ctx = cl.create_some_context(interactive=True)
    queue = cl.CommandQueue(ctx)

    #   create an array for the output
    print(q.shape)
    output = np.empty(q.shape, dtype=np.uint16)

    mf = cl.mem_flags
    q_opencl = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=q)
    output_opencl = cl.Buffer(ctx, mf.WRITE_ONLY, output.nbytes)

    prg = cl.Program(ctx, read_opencl_source("mandelbrot.cl")).build()

    prg.mandelbrot(queue, output.shape, None, q_opencl,
                   output_opencl, np.uint16(max_iter))

    cl.enqueue_copy(queue, output, output_opencl).wait()

    return output


# def do_calc(x1, x2, y1, y2, w, h):
#     xx = np.arange(x1, x2, (x2-x1)/w)
#     yy = np.arange(y2, y1, (y1-y2)/h) * 1j
#     q = np.ravel(xx+yy[:, np.newaxis]).astype(np.complex64)
#
#     return calc_fractal_opencl(q, 50)
#
# mandelbrot = do_calc(-2.5, 1.5, -1.5, 1.5, 500, 500)
#
# print(mandelbrot)